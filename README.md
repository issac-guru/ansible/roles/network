Ansible Role Host Management for Linux Systems
==============================================

A role for configuring ssh service on Debian based distributions using [Ansible](http://www.ansible.com/)
This Ansible role is part of [Issac Guru Project: Ansible Automation Script for Linux Environments](https://gitlab.com/issac-guru/ansible/linux/).


Features
--------

- Update apt
- Clean up apt
- Update packages
- Install packages


Requirements
------------

- Ansible 2.10.8 or higher
- Debian based distribution (may work with other versions, but has never been tested)


Role Variables
--------------

| VARIABLE                  | DESCRIPTION                                                             | DEFAULT       | OPTIONS             | EXAMPLE
|---------------------------|-------------------------------------------------------------------------|---------------|---------------------|----------------|
|`default_network_domain`   | Domain name used on hosts.                                              |               |                     | "issac.local"   |
|`generate_from_inventory`  | Generate */etc/hosts* file based on current ansible's inventory file.   |`"true"`       |`["true", "false"]`  |               |
|`generate_from_host_list`  | Generate */etc/hosts* file based on `host_list` array.                  |`"true"`       |`["true", "false"]`  |               |
|`host_list`                | List of hosts file                                                      |               |                    |   


Dependencies
------------

None


Example Playbook
----------------

The following example explains how-to consume ansible role:

```yml
  - name: "Host Management Example"
    hosts: all
    gather_facts: no
    ignore_errors: True

    vars:
      default_network_fqdn: "issac.local"

    roles:
    - {
        role: host-mgt,
        generate_from_inventory: true,
        generate_from_hosts_list: true,
        host_list:
        {
#         SERVER HOSTS
          "host-a": { ip_address: "192.168.0.2", hostname: "host-a", fqdn: "host-a.subdomain.issac.local" },
          "host-b": { ip_address: "192.168.0.3", hostname: "host-b", fqdn: "host-b.subdomain.issac.local" },
#         DESKTOP HOSTS
          "host-c": { ip_address: "192.168.0.4", hostname: "host-c", fqdn: "host-c.subdomain.issac.local" }
        }
      }
```


License
-------

The MIT License (MIT)

Copyright (c) 2022 Issac Guru

Premission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS-IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILIY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


Author Information
------------------

CREATED BY *Issac Nolis Ohasi* \
*LinkedIn Profile:* https://linkedin.com/in/ohasi \
*Gitlab Profile:* https://gitlab.com/ohasi/ \
*Website:* https://issac.guru \
*Contact:* me AT issac.guru
